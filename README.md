# start_project

[まず最初にお読みください](https://gitlab.com/docodemodem/for_developer/start_project/-/wikis/1.%E3%81%BE%E3%81%9A%E6%9C%80%E5%88%9D%E3%81%AB%E3%81%8A%E8%AA%AD%E3%81%BF%E3%81%8F%E3%81%A0%E3%81%95%E3%81%84)

[開発環境の準備](https://gitlab.com/docodemodem/for_developer/start_project/-/wikis/2.%E9%96%8B%E7%99%BA%E7%92%B0%E5%A2%83%E3%81%AE%E6%BA%96%E5%82%99)

[新規プロジェクトの作成](https://gitlab.com/docodemodem/for_developer/start_project/-/wikis/3.%E6%96%B0%E8%A6%8F%E3%83%97%E3%83%AD%E3%82%B8%E3%82%A7%E3%82%AF%E3%83%88%E3%81%AE%E4%BD%9C%E6%88%90)

[特定小電力無線の使い方](https://gitlab.com/docodemodem/for_developer/start_project/-/wikis/4.%E7%89%B9%E5%AE%9A%E5%B0%8F%E9%9B%BB%E5%8A%9B%E7%84%A1%E7%B7%9A%E3%81%AE%E4%BD%BF%E3%81%84%E6%96%B9)

[プロジェクトの書き込み方](https://gitlab.com/docodemodem/for_developer/start_project/-/wikis/5.%E3%83%97%E3%83%AD%E3%82%B8%E3%82%A7%E3%82%AF%E3%83%88%E3%81%AE%E6%9B%B8%E3%81%8D%E8%BE%BC%E3%81%BF%E6%96%B9)

[どこでもでむMINIの省エネ動作](https://gitlab.com/docodemodem/for_developer/start_project/-/wikis/6.%E3%81%A9%E3%81%93%E3%81%A7%E3%82%82%E3%81%A7%E3%82%80MINI%E3%81%AE%E7%9C%81%E3%82%A8%E3%83%8D%E5%8B%95%E4%BD%9C)
